// Array.prototype.myMap = function (callback) {
//   const newArray = [];
//   // Cambia solo el código debajo de esta línea
//   for (let i = 0; i < this.length; i++) {
//     newArray.push(callback(this[i]));
//   }
//   // Cambia solo el código encima de esta línea
//   return newArray;
// };

// const new_s = s.myMap(function (item) {
//   return item * 2;
// });

// var options = [
//   { name: "One", assigned: true },
//   { name: "Two", assigned: false },
//   { name: "Three", assigned: true },
//   { name: "Java", assigned: true },
// ];
// //Con ES6 puedes hacerlo muy corto:

// const O = options.filter((opt) => !opt.assigned).map((item) => item);
// //console.log(O);

const list = [
  { id: "001", nombre: "HARINA", precio: 3.3 },
  { id: "002", nombre: "ARROZ", precio: 2.5 },
  { id: "003", nombre: "PASTA", precio: 8.5 },
  { id: "004", nombre: "CARAOTAS", precio: 1.5 },
  { id: "005", nombre: "AZUCAR", precio: 4.5 },
];

const factura = {
  nro: 1,
  id: "001",
  nombre: "Pasta",
  precio: 5.5,
};

const factura2 = {
  nro: 2,
  id: "002",
  nombre: "ARROZ",
  precio: 3.5,
};
const factura3 = {
  nro: 3,
  id: "003",
  nombre: "azucar",
  precio: 4.5,
};


function saveFactura(param) {
  if (localStorage.getItem("Facturas") === null) {
    let facturas = [];
    facturas.push(param);
    localStorage.setItem("Facturas", JSON.stringify(facturas));
  } else {
    let facturas = JSON.parse(localStorage.getItem("Facturas"));
    facturas.push(param);
    localStorage.setItem("Facturas", JSON.stringify(facturas));
  }
}


function getFactura() {
  if (localStorage !== null) {
    const facturas = JSON.parse(localStorage.getItem("Facturas"));
    // console.log(facturas);
    return facturas;
  } return console.log('ERROR 404 FILE NOT FOUND');
}
function findFactura(arr) {
  let facturaToshow = arr.filter(item => item.nro == 1);
  return facturaToshow;
}
console.log(findFactura(getFactura()));
// saveFactura(factura3);
getFactura();